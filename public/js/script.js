
var tags = document.querySelectorAll(".filter-tag input");
var cards = document.getElementsByClassName("col-card ");  
var prev = null;


// number of cards in tags
countCardsByTag();

// tag switch
for (var i = 0; i < tags.length; i++) {

    tags[i].addEventListener('change', function() {
        // (prev) ? console.log(prev.value): null;
        if (this !== prev) {
            prev = this;
        }
        filterCardsByTag(prev.value)   
        
    });
}




function filterCardsByTag(tag) {
    if (tag == "all") {
        for (var i = 0; i < cards.length; i++) {
            cards[i].style.display ="block";
        }
        return;
    }
    
    for (var i = 0; i < cards.length; i++) {
        if(cards[i].classList.contains(tag)) {
            cards[i].style.display ="block";
        } else {
            cards[i].style.display ="none";
        }  
    }
}

function countCardsByTag() {
    for (var i = 0; i < tags.length; i++) {
        var tag = tags[i].value;
        var btn = tags[i].nextElementSibling;
        
        if(tag != 'all') {
            var num = document.querySelectorAll('.' + tag).length;
            var string = '<span class="number">&nbsp;' + num + '</span>';
            btn.insertAdjacentHTML("beforeend", string);
        }
    }
}

