# envelop-ecosystem



## We use gulp.js

To start the project:
go to the project's folder 
and run

```
npm install
gulp

```

## data.json

The data.json is stored in the js folder

### Dev & Build with docker
```bash
# Local run
docker run -it --rm  -v $PWD:/app node:18 /bin/bash -c 'cd /app && npm install && npx gulp'
##or

docker run -it --rm  -v $PWD:/app node:18 /bin/bash -c 'cd /app && npm install && npm run dev'

#production build
docker run -it --rm  -v $PWD:/app node:18 /bin/bash -c 'cd /app && npm install && npm run build'
```